# Macaon Documentation

## Overview
Macaon is a trainable software whose purpose is to annotate text.\
It has been built to perform any combination of the folloing annotations :
* Tokenization
* POS tagging
* Feats tagging
* Lemmatization
* Dependency parsing
* Sentence segmentation

The focus is set on customization : 
* Chose the order the predictions are made in, along with the mode of annotation : 
	* Sequential (pipeline) mode : the whole input is processed at a certain annotation level **n** before being processed at level **n+1**.
	* Incremental mode : each word is processed at every anotation level before passing to the next word.
* Precisely chose what part of the input text will be used as feature for the classifier.

## [Installation](documentation/install.md)
## [Getting Started](documentation/gettingStarted.md)

