#ifndef MACAONTRAIN__H
#define MACAONTRAIN__H

#include <boost/program_options.hpp>
#include "Trainer.hpp"
#include "Decoder.hpp"

namespace po = boost::program_options;

class MacaonTrain
{

  private :

  int argc;
  char ** argv;

  private :

  po::options_description getOptionsDescription();
  po::variables_map checkOptions(po::options_description & od);

  private :

  Trainer::TrainStrategy parseTrainStrategy(std::string s);

  public :

  MacaonTrain(int argc, char ** argv);
  int main();
};

#endif
