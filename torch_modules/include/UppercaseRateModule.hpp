#ifndef UPPERCASERATEMODULE__H
#define UPPERCASERATEMODULE__H

#include <torch/torch.h>
#include "Submodule.hpp"
#include "MyModule.hpp"
#include "LSTM.hpp"
#include "GRU.hpp"
#include "Concat.hpp"

class UppercaseRateModuleImpl : public Submodule
{
  private :

  int outSize;
  std::vector<int> focusedBuffer, focusedStack;
  std::shared_ptr<MyModule> myModule{nullptr};

  public :

  UppercaseRateModuleImpl(std::string name, const std::string & definition);
  torch::Tensor forward(torch::Tensor input);
  std::size_t getOutputSize() override;
  std::size_t getInputSize() override;
  void addToContext(torch::Tensor & context, const Config & config) override;
  void registerEmbeddings(bool loadPretrained) override;
};
TORCH_MODULE(UppercaseRateModule);

#endif

