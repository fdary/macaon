#ifndef DISTANCEMODULE__H
#define DISTANCEMODULE__H

#include <torch/torch.h>
#include "Submodule.hpp"
#include "MyModule.hpp"
#include "LSTM.hpp"
#include "GRU.hpp"
#include "Concat.hpp"
#include "WordEmbeddings.hpp"

class DistanceModuleImpl : public Submodule
{
  private :

  WordEmbeddings wordEmbeddings{nullptr};
  std::shared_ptr<MyModule> myModule{nullptr};
  std::vector<int> fromBuffer, fromStack;
  std::vector<int> toBuffer, toStack;
  int threshold;
  int inSize;

  public :

  DistanceModuleImpl(std::string name, const std::string & definition);
  torch::Tensor forward(torch::Tensor input);
  std::size_t getOutputSize() override;
  std::size_t getInputSize() override;
  void addToContext(torch::Tensor & context, const Config & config) override;
  void registerEmbeddings(bool loadPretrained) override;
};
TORCH_MODULE(DistanceModule);

#endif

