#ifndef MYMODULE__H
#define MYMODULE__H

#include <torch/torch.h>

class MyModule : public torch::nn::Module
{
  public :

  struct ModuleOptions : std::tuple<bool,bool,int,float,bool>
  {
    ModuleOptions(bool batchFirst){std::get<0>(*this)=batchFirst;};
    ModuleOptions & bidirectional(bool val) {std::get<1>(*this)=val; return *this;}
    ModuleOptions & num_layers(int num) {std::get<2>(*this)=num; return *this;}
    ModuleOptions & dropout(float val) {std::get<3>(*this)=val; return *this;}
    ModuleOptions & complete(bool val) {std::get<4>(*this)=val; return *this;}
  };

  public :

  virtual int getOutputSize(int sequenceLength) = 0;
  virtual torch::Tensor forward(torch::Tensor) = 0;
};

#endif
