#ifndef CONTEXTUALMODULE__H
#define CONTEXTUALMODULE__H

#include <torch/torch.h>
#include <optional>
#include "Submodule.hpp"
#include "MyModule.hpp"
#include "GRU.hpp"
#include "LSTM.hpp"
#include "Concat.hpp"
#include "WordEmbeddings.hpp"

class ContextualModuleImpl : public Submodule
{
  private :

  WordEmbeddings wordEmbeddings{nullptr};
  std::shared_ptr<MyModule> myModule{nullptr};
  std::vector<std::string> columns;
  std::vector<std::function<std::string(const std::string &)>> functions;
  std::vector<std::tuple<Config::Object, int, std::optional<int>>> targets;
  std::pair<int,int> window;
  int inSize;
  int outSize;
  std::filesystem::path path;
  std::filesystem::path w2vFiles;

  public :

  ContextualModuleImpl(std::string name, const std::string & definition, std::filesystem::path path);
  torch::Tensor forward(torch::Tensor input);
  std::size_t getOutputSize() override;
  std::size_t getInputSize() override;
  void addToContext(torch::Tensor & context, const Config & config) override;
  void registerEmbeddings(bool loadPretrained) override;
};
TORCH_MODULE(ContextualModule);

#endif

