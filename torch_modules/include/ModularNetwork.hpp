#ifndef MODULARNETWORK__H
#define MODULARNETWORK__H

#include "NeuralNetwork.hpp"
#include "ContextModule.hpp"
#include "ContextualModule.hpp"
#include "RawInputModule.hpp"
#include "SplitTransModule.hpp"
#include "AppliableTransModule.hpp"
#include "FocusedColumnModule.hpp"
#include "DepthLayerTreeEmbeddingModule.hpp"
#include "StateNameModule.hpp"
#include "UppercaseRateModule.hpp"
#include "NumericColumnModule.hpp"
#include "HistoryModule.hpp"
#include "HistoryMineModule.hpp"
#include "DistanceModule.hpp"
#include "MLP.hpp"

class ModularNetworkImpl : public NeuralNetworkImpl
{
  private :

  torch::nn::Dropout inputDropout{nullptr};

  MLP mlp{nullptr};
  std::vector<std::shared_ptr<Submodule>> modules;
  std::map<std::string,torch::nn::Linear> outputLayersPerState;
  std::size_t totalInputSize{0};

  public :

  ModularNetworkImpl(std::string name, std::map<std::string,std::size_t> nbOutputsPerState, std::vector<std::string> definitions, std::filesystem::path path);
  torch::Tensor forward(torch::Tensor input, const std::string & state) override;
  torch::Tensor extractContext(Config & config) override;
  void registerEmbeddings(bool loadPretrained) override;
  void saveDicts(std::filesystem::path path) override;
  void loadDicts(std::filesystem::path path) override;
  void setDictsState(Dict::State state) override;
  void setCountOcc(bool countOcc) override;
  void removeRareDictElements(float rarityThreshold) override;
};

#endif
