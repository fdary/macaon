#ifndef FOCUSEDCOLUMNMODULE__H
#define FOCUSEDCOLUMNMODULE__H

#include <torch/torch.h>
#include "Submodule.hpp"
#include "MyModule.hpp"
#include "LSTM.hpp"
#include "GRU.hpp"
#include "Concat.hpp"
#include "WordEmbeddings.hpp"

class FocusedColumnModuleImpl : public Submodule
{
  private :

  WordEmbeddings wordEmbeddings{nullptr};
  std::shared_ptr<MyModule> myModule{nullptr};
  std::vector<int> focusedBuffer, focusedStack;
  std::string column;
  std::function<std::string(const std::string&)> func{[](const std::string &s){return s;}};
  int maxNbElements;
  int inSize;
  std::filesystem::path path;
  std::filesystem::path w2vFiles;

  public :

  FocusedColumnModuleImpl(std::string name, const std::string & definition, std::filesystem::path path);
  torch::Tensor forward(torch::Tensor input);
  std::size_t getOutputSize() override;
  std::size_t getInputSize() override;
  void addToContext(torch::Tensor & context, const Config & config) override;
  void registerEmbeddings(bool loadPretrained) override;
};
TORCH_MODULE(FocusedColumnModule);

#endif

