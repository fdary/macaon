#ifndef STATENAMEMODULE__H
#define STATENAMEMODULE__H

#include <torch/torch.h>
#include "Submodule.hpp"
#include "MyModule.hpp"
#include "LSTM.hpp"
#include "GRU.hpp"
#include "WordEmbeddings.hpp"

class StateNameModuleImpl : public Submodule
{
  private :

  WordEmbeddings embeddings{nullptr};
  int outSize;

  public :

  StateNameModuleImpl(std::string name, const std::string & definition);
  torch::Tensor forward(torch::Tensor input);
  std::size_t getOutputSize() override;
  std::size_t getInputSize() override;
  void addToContext(torch::Tensor & context, const Config & config) override;
  void registerEmbeddings(bool loadPretrained) override;
};
TORCH_MODULE(StateNameModule);

#endif

