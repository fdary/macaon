#ifndef LOSSFUNCTION__H
#define LOSSFUNCTION__H

#include <variant>
#include "torch/torch.h"
#include "CustomHingeLoss.hpp"

class LossFunction
{
  private :

  std::string name{"_undefined_loss_"};
  std::variant<torch::nn::CrossEntropyLoss, torch::nn::BCELoss, torch::nn::MSELoss, CustomHingeLoss, torch::nn::L1Loss> fct;

  public :

  void init(std::string name);
  torch::Tensor operator()(torch::Tensor prediction, torch::Tensor gold);
  torch::Tensor getGoldFromClassesIndexes(int nbClasses, const std::vector<long> & goldIndexes) const;
};

#endif

