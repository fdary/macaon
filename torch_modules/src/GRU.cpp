#include "GRU.hpp"

GRUImpl::GRUImpl(int inputSize, int outputSize, ModuleOptions options) : outputAll(std::get<4>(options))
{
  auto gruOptions = torch::nn::GRUOptions(inputSize, std::get<1>(options) ? outputSize/2 : outputSize)
    .batch_first(std::get<0>(options))
    .bidirectional(std::get<1>(options))
    .num_layers(std::get<2>(options))
    .dropout(std::get<3>(options));

  gru = register_module("gru", torch::nn::GRU(gruOptions));
}

torch::Tensor GRUImpl::forward(torch::Tensor input)
{
  return std::get<0>(gru(input));
}

int GRUImpl::getOutputSize(int sequenceLength)
{
  if (outputAll)
    return sequenceLength * gru->options.hidden_size() * (gru->options.bidirectional() ? 2 : 1);

  return gru->options.hidden_size() * (gru->options.bidirectional() ? 4 : 1);
}

