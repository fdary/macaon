#include "ContextModule.hpp"

ContextModuleImpl::ContextModuleImpl(std::string name, const std::string & definition, std::filesystem::path path) : path(path)
{
  setName(name);

  std::regex regex("(?:(?:\\s|\\t)*)Targets\\{(.*)\\}(?:(?:\\s|\\t)*)Columns\\{(.*)\\}(?:(?:\\s|\\t)*)(\\S+)\\{(.*)\\}(?:(?:\\s|\\t)*)In\\{(.*)\\}(?:(?:\\s|\\t)*)Out\\{(.*)\\}(?:(?:\\s|\\t)*)w2v\\{(.*)\\}(?:(?:\\s|\\t)*)");
  if (!util::doIfNameMatch(regex, definition, [this,&definition](auto sm)
        {
          try
          {
            for (auto & target : util::split(sm.str(1), ' '))
            {
              auto splited = util::split(target, '.');
              if (splited.size() != 2 and splited.size() != 3)
                util::myThrow(fmt::format("invalid target '{}' expected 'object.index(.childIndex)'", target));
              targets.emplace_back(std::make_tuple(Config::str2object(splited[0]), std::stoi(splited[1]), splited.size() == 3 ? std::optional<int>(std::stoi(splited[2])) : std::optional<int>()));
            }

            auto funcColumns = util::split(sm.str(2), ' ');
            columns.clear();
            for (auto & funcCol : funcColumns)
            {
              functions.emplace_back() = getFunction(funcCol);
              columns.emplace_back(util::split(funcCol, ':').back());
            }

            auto subModuleType = sm.str(3);
            auto subModuleArguments = util::split(sm.str(4), ' ');

            auto options = MyModule::ModuleOptions(true)
              .bidirectional(std::stoi(subModuleArguments[0]))
              .num_layers(std::stoi(subModuleArguments[1]))
              .dropout(std::stof(subModuleArguments[2]))
              .complete(std::stoi(subModuleArguments[3]));

            inSize = std::stoi(sm.str(5));
            int outSize = std::stoi(sm.str(6));

            if (subModuleType == "LSTM")
              myModule = register_module("myModule", LSTM(columns.size()*inSize, outSize, options));
            else if (subModuleType == "GRU")
              myModule = register_module("myModule", GRU(columns.size()*inSize, outSize, options));
            else if (subModuleType == "Concat")
              myModule = register_module("myModule", Concat(columns.size()*inSize, outSize));
            else if (subModuleType == "Transformer")
              myModule = register_module("myModule", Transformer(columns.size()*inSize, outSize, options));
            else
              util::myThrow(fmt::format("unknown sumodule type '{}'", subModuleType));

            w2vFiles = sm.str(7);

            if (!w2vFiles.empty())
            {
              auto pathes = util::split(w2vFiles.string(), ' ');
              for (auto & p : pathes)
              {
                auto splited = util::split(p, ',');
                if (splited.size() != 2)
                  util::myThrow("expected 'prefix,pretrained.w2v'");

                auto pretrained = getDict().loadWord2Vec(this->path / splited[1], splited[0]);
                if (pretrained)
                {
                  getDict().setState(Dict::State::Closed);
                  dictSetPretrained(true);
                }
              }
            }

          } catch (std::exception & e) {util::myThrow(fmt::format("{} in '{}'",e.what(),definition));}
        }))
    util::myThrow(fmt::format("invalid definition '{}'", definition));
}

std::size_t ContextModuleImpl::getOutputSize()
{
  return myModule->getOutputSize(targets.size());
}

std::size_t ContextModuleImpl::getInputSize()
{
  return columns.size()*(targets.size());
}

void ContextModuleImpl::addToContext(torch::Tensor & context, const Config & config)
{
  auto & dict = getDict();
  std::vector<long> contextIndexes;

  for (auto & target : targets)
    if (config.hasRelativeWordIndex(std::get<0>(target), std::get<1>(target)))
    {
      int baseIndex = config.getRelativeWordIndex(std::get<0>(target), std::get<1>(target));
      if (!std::get<2>(target))
        contextIndexes.emplace_back(baseIndex);
      else
      {
        int childIndex = *std::get<2>(target);
        auto childs = util::split(std::string(config.getAsFeature(Config::childsColName, baseIndex)), '|');
        int candidate = -2;

        if (childIndex >= 0 and childIndex < (int)childs.size())
        {
          candidate = std::stoi(childs[childIndex]);
          if (candidate > baseIndex)
            candidate = -2;
        }
        else if (childIndex < 0 and ((int)childs.size())+childIndex >= 0)
        {
          candidate = std::stoi(childs[childs.size()+childIndex]);
          if (candidate < baseIndex)
            candidate = -2;
        }

        contextIndexes.emplace_back(candidate);
      }
    }
    else if (std::get<0>(target) == Config::Object::Stack)
    {
      contextIndexes.emplace_back(-1);
    }
    else
    {
      contextIndexes.emplace_back(-3);
    }

  int insertIndex = 0;
  for (auto index : contextIndexes)
    for (unsigned int colIndex = 0; colIndex < columns.size(); colIndex++)
    {
      auto & col = columns[colIndex];
      if (index == -1)
      {
        context[firstInputIndex+insertIndex] = dict.getIndexOrInsert(Dict::nullValueStr, col);
      }
      else if (index == -2)
      {
        context[firstInputIndex+insertIndex] = dict.getIndexOrInsert(Dict::noChildValueStr, col);
      }
      else if (index == -3)
      {
        context[firstInputIndex+insertIndex] = dict.getIndexOrInsert(Dict::oobValueStr, col);
      }
      else
      {
        int dictIndex;
        if (col == Config::idColName)
        {
          std::string value;
          if (config.getAsFeature(Config::idColName, index).empty())
            value = "empty";
          else if (config.isMultiwordPredicted(index))
            value = "multiword";
          else if (config.getAsFeature(Config::isMultiColName, index) == Config::EOSSymbol1)
            value = "part";
          else if (config.isTokenPredicted(index))
            value = "token";
          else
          {
            config.printForDebug(stderr);
            util::myThrow(fmt::format("{} col at index {} not token nor multiword", Config::idColName, index));
          }
          dictIndex = dict.getIndexOrInsert(value, col);
        }
        else
        {
          std::string featureValue = functions[colIndex](config.getAsFeature(col, index));
          dictIndex = dict.getIndexOrInsert(featureValue, col);
        }

        context[firstInputIndex+insertIndex] = dictIndex;
      }
      insertIndex++;
    }
}

torch::Tensor ContextModuleImpl::forward(torch::Tensor input)
{
  auto context = wordEmbeddings(input.narrow(1, firstInputIndex, getInputSize()));

  context = context.view({context.size(0), context.size(1)/(int)columns.size(), (int)columns.size()*context.size(2)});

  return myModule->forward(context).reshape({input.size(0), -1});
}

void ContextModuleImpl::registerEmbeddings(bool loadPretrained)
{
  if (!wordEmbeddings)
    wordEmbeddings = register_module("embeddings", WordEmbeddings(getDict().size(), inSize,  getDict().getSpecialIndexes()));
  auto pathes = util::split(w2vFiles.string(), ' ');
  for (auto & p : pathes)
  {
    auto splited = util::split(p, ',');
    loadPretrainedW2vEmbeddings(wordEmbeddings->getNormalEmbeddings(), path / splited[1], splited[0], loadPretrained);
  }
}

