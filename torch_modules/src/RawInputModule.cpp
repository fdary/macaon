#include "RawInputModule.hpp"

RawInputModuleImpl::RawInputModuleImpl(std::string name, const std::string & definition)
{
  setName(name);
  std::regex regex("(?:(?:\\s|\\t)*)Left\\{(.*)\\}(?:(?:\\s|\\t)*)Right\\{(.*)\\}(?:(?:\\s|\\t)*)(\\S+)\\{(.*)\\}(?:(?:\\s|\\t)*)In\\{(.*)\\}(?:(?:\\s|\\t)*)Out\\{(.*)\\}(?:(?:\\s|\\t)*)");
  if (!util::doIfNameMatch(regex, definition, [this,&definition](auto sm)
        {
          try
          {
            leftWindow = std::stoi(sm.str(1));
            rightWindow = std::stoi(sm.str(2));

            if (leftWindow < 0 or rightWindow < 0)
              util::myThrow(fmt::format("Invalid negative values for leftWindow({}) or rightWindow({})", leftWindow, rightWindow));

            auto subModuleType = sm.str(3);
            auto subModuleArguments = util::split(sm.str(4), ' ');

            auto options = MyModule::ModuleOptions(true)
              .bidirectional(std::stoi(subModuleArguments[0]))
              .num_layers(std::stoi(subModuleArguments[1]))
              .dropout(std::stof(subModuleArguments[2]))
              .complete(std::stoi(subModuleArguments[3]));

            inSize = std::stoi(sm.str(5));
            int outSize = std::stoi(sm.str(6));

            if (subModuleType == "LSTM")
              myModule = register_module("myModule", LSTM(inSize, outSize, options));
            else if (subModuleType == "GRU")
              myModule = register_module("myModule", GRU(inSize, outSize, options));
            else if (subModuleType == "Concat")
              myModule = register_module("myModule", Concat(inSize, outSize));
            else if (subModuleType == "Transformer")
              myModule = register_module("myModule", Transformer(inSize, outSize, options));
            else
              util::myThrow(fmt::format("unknown sumodule type '{}'", subModuleType));

          } catch (std::exception & e) {util::myThrow(fmt::format("{} in '{}'",e.what(),definition));}
        }))
    util::myThrow(fmt::format("invalid definition '{}'", definition));
}

torch::Tensor RawInputModuleImpl::forward(torch::Tensor input)
{
  return myModule->forward(wordEmbeddings(input.narrow(1, firstInputIndex, getInputSize()))).reshape({input.size(0), -1});
}

std::size_t RawInputModuleImpl::getOutputSize()
{
  return myModule->getOutputSize(leftWindow + rightWindow + 1);
}

std::size_t RawInputModuleImpl::getInputSize()
{
  return leftWindow + rightWindow + 1;
}

void RawInputModuleImpl::addToContext(torch::Tensor & context, const Config & config)
{
  std::string prefix = "LETTER";
  auto & dict = getDict();

  int insertIndex = 0;
  for (int i = 0; i < leftWindow; i++)
  {
    if (config.hasCharacter(config.getCharacterIndex()-leftWindow+i))
      context[firstInputIndex+insertIndex] = dict.getIndexOrInsert(fmt::format("{}", config.getLetter(config.getCharacterIndex()-leftWindow+i)), prefix);
    else
      context[firstInputIndex+insertIndex] = dict.getIndexOrInsert(Dict::nullValueStr, prefix);
  
    insertIndex++;
  }

  for (int i = 0; i <= rightWindow; i++)
  {
    if (config.hasCharacter(config.getCharacterIndex()+i))
      context[firstInputIndex+insertIndex] = dict.getIndexOrInsert(fmt::format("{}", config.getLetter(config.getCharacterIndex()+i)), prefix);
    else
      context[firstInputIndex+insertIndex] = dict.getIndexOrInsert(Dict::nullValueStr, prefix);

    insertIndex++;
  }
}

void RawInputModuleImpl::registerEmbeddings(bool)
{
  if (!wordEmbeddings)
    wordEmbeddings = register_module("embeddings", WordEmbeddings(getDict().size(), inSize, std::set<std::size_t>()));
}

