#include "WordEmbeddings.hpp"
#include "util.hpp"
#include "NeuralNetwork.hpp"

bool WordEmbeddingsImpl::scaleGradByFreq = false;
bool WordEmbeddingsImpl::canTrainPretrained = false;
float WordEmbeddingsImpl::maxNorm = std::numeric_limits<float>::max();

WordEmbeddingsImpl::WordEmbeddingsImpl(std::size_t vocab, std::size_t dim, std::set<std::size_t> specialIndexes)
{
  for (auto elem : specialIndexes)
  {
    if (elem >= specialIndexes.size())
      util::error("Special indexes are not contiguous from zero.");
  }
  if (maxNorm == std::numeric_limits<float>::max())
  {
    normalEmbeddings = register_module("normalEmbeddings", torch::nn::Embedding(torch::nn::EmbeddingOptions(vocab, dim).scale_grad_by_freq(scaleGradByFreq)));
    specialEmbeddings = register_module("specialEmbeddings", torch::nn::Embedding(torch::nn::EmbeddingOptions(specialIndexes.size(), dim).scale_grad_by_freq(scaleGradByFreq)));
  }
  else
  {
    normalEmbeddings = register_module("normalEmbeddings", torch::nn::Embedding(torch::nn::EmbeddingOptions(vocab, dim).max_norm(maxNorm).scale_grad_by_freq(scaleGradByFreq)));
    specialEmbeddings = register_module("specialEmbeddings", torch::nn::Embedding(torch::nn::EmbeddingOptions(specialIndexes.size(), dim).scale_grad_by_freq(scaleGradByFreq)));
  }
}

torch::nn::Embedding WordEmbeddingsImpl::getNormalEmbeddings()
{
  return normalEmbeddings;
}

void WordEmbeddingsImpl::setScaleGradByFreq(bool scaleGradByFreq)
{
  WordEmbeddingsImpl::scaleGradByFreq = scaleGradByFreq;
}

void WordEmbeddingsImpl::setMaxNorm(float maxNorm)
{
  WordEmbeddingsImpl::maxNorm = maxNorm;
}

void WordEmbeddingsImpl::setCanTrainPretrained(bool value)
{
  WordEmbeddingsImpl::canTrainPretrained = value;
}

torch::Tensor WordEmbeddingsImpl::forward(torch::Tensor input)
{
  if (specialEmbeddings->weight.size(0) == 0)
    return normalEmbeddings(input);

  auto mask = input >= specialEmbeddings->weight.size(0);
  auto specialIndexes = torch::ones(input.sizes(),torch::TensorOptions(torch::kLong).device(NeuralNetworkImpl::getDevice()));
  specialIndexes.index_put_({mask}, 0);
  auto normalRes = normalEmbeddings(input);
  auto specialRes = specialEmbeddings(input * specialIndexes);
  auto normalIndexes = torch::ones(normalRes.sizes(),torch::TensorOptions(torch::kLong).device(NeuralNetworkImpl::getDevice()));
  specialIndexes = torch::ones(specialRes.sizes(),torch::TensorOptions(torch::kLong).device(NeuralNetworkImpl::getDevice()));
  specialIndexes.index_put_({mask}, 0);
  normalIndexes.index_put_({~mask}, 0);

  return normalIndexes*normalRes + specialIndexes*specialRes;
}

bool WordEmbeddingsImpl::getCanTrainPretrained()
{
  return canTrainPretrained;
}

