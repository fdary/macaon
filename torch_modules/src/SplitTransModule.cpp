#include "SplitTransModule.hpp"
#include "Transition.hpp"

SplitTransModuleImpl::SplitTransModuleImpl(std::string name, int maxNbTrans, const std::string & definition)
{
  setName(name);
  this->maxNbTrans = maxNbTrans;
  std::regex regex("(?:(?:\\s|\\t)*)(\\S+)\\{(.*)\\}(?:(?:\\s|\\t)*)In\\{(.*)\\}(?:(?:\\s|\\t)*)Out\\{(.*)\\}(?:(?:\\s|\\t)*)");
  if (!util::doIfNameMatch(regex, definition, [this,&definition](auto sm)
        {
          try
          {
            auto subModuleType = sm.str(1);
            auto subModuleArguments = util::split(sm.str(2), ' ');

            auto options = MyModule::ModuleOptions(true)
              .bidirectional(std::stoi(subModuleArguments[0]))
              .num_layers(std::stoi(subModuleArguments[1]))
              .dropout(std::stof(subModuleArguments[2]))
              .complete(std::stoi(subModuleArguments[3]));

            inSize = std::stoi(sm.str(3));
            int outSize = std::stoi(sm.str(4));

            if (subModuleType == "LSTM")
              myModule = register_module("myModule", LSTM(inSize, outSize, options));
            else if (subModuleType == "GRU")
              myModule = register_module("myModule", GRU(inSize, outSize, options));
            else if (subModuleType == "Concat")
              myModule = register_module("myModule", Concat(inSize, outSize));
            else
              util::myThrow(fmt::format("unknown sumodule type '{}'", subModuleType));

          } catch (std::exception & e) {util::myThrow(fmt::format("{} in '{}'",e.what(),definition));}
        }))
    util::myThrow(fmt::format("invalid definition '{}'", definition));
}

torch::Tensor SplitTransModuleImpl::forward(torch::Tensor input)
{
  return myModule->forward(wordEmbeddings(input.narrow(1, firstInputIndex, getInputSize()))).reshape({input.size(0), -1});
}

std::size_t SplitTransModuleImpl::getOutputSize()
{
  return myModule->getOutputSize(maxNbTrans);
}

std::size_t SplitTransModuleImpl::getInputSize()
{
  return maxNbTrans;
}

void SplitTransModuleImpl::addToContext(torch::Tensor & context, const Config & config)
{
  auto & dict = getDict();
  auto & splitTransitions = config.getAppliableSplitTransitions();
  for (int i = 0; i < maxNbTrans; i++)
    if (i < (int)splitTransitions.size())
      context[firstInputIndex+i] = dict.getIndexOrInsert(splitTransitions[i]->getName(), "");
    else
      context[firstInputIndex+i] = dict.getIndexOrInsert(Dict::nullValueStr, "");
}

void SplitTransModuleImpl::registerEmbeddings(bool)
{
  if (!wordEmbeddings)
    wordEmbeddings = register_module("embeddings", WordEmbeddings(getDict().size(), inSize, std::set<std::size_t>()));
}

