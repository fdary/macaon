#ifndef ACTION__H
#define ACTION__H

#include <functional>
#include <string>
#include <vector>
#include "Config.hpp"

class Action
{
  public :

  enum class Type
  {
    Push,
    Pop,
    Write,
    MoveWord,
    MoveChar,
    AddLines,
    Check
  };

  private :

  Type type;
  std::vector<std::string> data;

  public :

  std::function<void(Config & config, Action & action)> apply;
  std::function<void(Config & config, Action & action)> undo;
  std::function<bool(const Config & config, const Action & action)> appliable;

  private :

  Action(Type type, std::function<void(Config & config, Action & action)> apply, std::function<void(Config & config, Action & action)> undo, std::function<bool(const Config & config, const Action & action)> appliable);

  public :

  static Action addLinesIfNeeded(int nbLines);
  static Action moveWordIndex(int movement);
  static Action moveCharacterIndex(int movement);
  static Action addHypothesis(const std::string & colName, std::size_t lineIndex, const std::string & hypothesis);
  static Action addToHypothesis(const std::string & colName, std::size_t lineIndex, const std::string & addition);
  static Action sumToHypothesis(const std::string & colName, std::size_t lineIndex, float addition, bool mean);
  static Action maxWithHypothesis(const std::string & colName, std::size_t lineIndex, float addition);
  static Action addHypothesisRelative(const std::string & colName, Config::Object object, int relativeIndex, const std::string & hypothesis);
  static Action addHypothesisRelativeRelaxed(const std::string & colName, Config::Object object, int relativeIndex, const std::string & hypothesis);
  static Action addToHypothesisRelative(const std::string & colName, Config::Object object, int relativeIndex, const std::string & addition);
  static Action writeScore(const std::string & colName, Config::Object object, int relativeIndex);
  static Action pushWordIndexOnStack();
  static Action popStack(int relIndex);
  static Action emptyStack();
  static Action setRoot(int bufferIndex);
  static Action updateIds(int bufferIndex);
  static Action endWord();
  static Action assertIsEmpty(const std::string & colName, Config::Object object, int relativeIndex);
  static Action assertIsNotEmpty(const std::string & colName, Config::Object object, int relativeIndex);
  static Action attach(Config::Object governorObject, int governorIndex, Config::Object dependentObject, int dependentIndex);
  static Action addCharsToCol(const std::string & col, int n, Config::Object object, int relativeIndex);
  static Action ignoreCurrentCharacter();
  static Action consumeCharacterIndex(const util::utf8string & consumed);
  static Action setMultiwordIds(int multiwordSize);
  static Action split(int index);
  static Action setRootUpdateIdsEmptyStackIfSentChanged();
  static Action deprel(std::string value);
  static Action transformSuffix(std::string fromCol, Config::Object fromObj, int fromIndex, std::string toCol, Config::Object toObj, int toIndex, util::utf8string toRemove, util::utf8string toAdd);
  static Action copyContent(std::string fromCol, Config::Object fromObj, int fromIndex, std::string toCol, Config::Object toObj, int toIndex);
  static Action uppercase(std::string col, Config::Object obj, int index);
  static Action uppercaseIndex(std::string col, Config::Object obj, int index, int inIndex);
  static Action lowercase(std::string col, Config::Object obj, int index);
  static Action lowercaseIndex(std::string col, Config::Object obj, int index, int inIndex);
};

#endif
