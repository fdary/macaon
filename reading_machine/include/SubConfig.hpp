#ifndef SUBCONFIG__H
#define SUBCONFIG__H

#include "Config.hpp"
#include "BaseConfig.hpp"

class SubConfig : public Config
{
  private :

  const BaseConfig & model;
  std::size_t spanSize;
  std::size_t firstLineIndex{0};

  public :

  SubConfig(BaseConfig & model, std::size_t spanSize);
  bool update();
  bool needsUpdate();
  std::size_t getNbColumns() const override;
  std::size_t getColIndex(const std::string & colName) const override;
  bool hasColIndex(const std::string & colName) const override;
  const std::string & getColName(int colIndex) const override;
  std::size_t getFirstLineIndex() const override;
};

#endif
