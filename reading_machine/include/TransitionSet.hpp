#ifndef TRANSITIONSET__H
#define TRANSITIONSET__H

#include <vector>
#include <string>
#include <unordered_map>
#include "Transition.hpp"

class TransitionSet
{
  private :

  std::vector<Transition> transitions;

  private :

  void addTransitionsFromFile(const std::string & filename);

  public :

  TransitionSet(const std::vector<std::string> & filenames);
  TransitionSet(const std::string & filename);
  std::vector<std::pair<Transition*, int>> getAppliableTransitionsCosts(const Config & c, bool dynamic = false);
  std::vector<Transition *> getBestAppliableTransitions(const Config & c, const std::vector<int> & appliableTransitions, bool dynamic = false);
  std::vector<Transition *> getNAppliableTransitions(const Config & c, int n);
  std::vector<int> getAppliableTransitions(const Config & c);
  std::size_t getTransitionIndex(const Transition * transition) const;
  Transition * getTransition(std::size_t index);
  Transition * getTransition(const std::string & name);
  std::size_t size() const;
  std::map<std::string, int> computeLinks(const Config & c);
};

#endif
