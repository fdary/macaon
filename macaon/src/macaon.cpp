#include "MacaonTrain.hpp"
#include "MacaonDecode.hpp"

void printUsageAndExit(char * argv[])
{
  fmt::print(stderr, "USAGE : {} (train | decode)\n", argv[0]);
  exit(1);
}

int main(int argc, char * argv[])
{
  if (argc < 2)
    printUsageAndExit(argv);

  if (argv[1] == std::string("train"))
  {
    MacaonTrain program(argc-1, argv+1);
    return program.main();
  }
  else if (argv[1] == std::string("decode"))
  {
    MacaonDecode program(argc-1, argv+1);
    return program.main();
  }
  else
    printUsageAndExit(argv);

  return 0;
}

