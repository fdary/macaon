#include "utf8string.hpp"
#include "util.hpp"

util::utf8char::utf8char()
{
  for (auto & val : (*this))
    val = '\0';
}

util::utf8char::utf8char(const std::string & other)
{
  *this = other;
}

util::utf8char::utf8char(const char * other)
{
  *this = std::string(other);
}

util::utf8char & util::utf8char::operator=(char other)
{
  (*this)[0] = other;
  return *this;
}

util::utf8char & util::utf8char::operator=(const std::string & other)
{
  auto splited = splitAsUtf8(other);
  if (splited.size() > 1)
    myThrow(fmt::format("Assigning invalid utf8 character '{}'", other));
  return *this = splited[0];
}

bool util::utf8char::operator==(const std::string & other) const
{
  auto splited = splitAsUtf8(other);
  if (splited.size() > 1)
    myThrow(fmt::format("Comparing with invalid utf8 character '{}'", other));
  return *this == splited[0];
}

bool util::utf8char::operator==(char other) const
{
  return (*this)[0] == other && !(*this)[1] && !(*this)[2] && !(*this)[3];
}

bool util::utf8char::operator!=(char other) const
{
  return ! (*this==other);
}

util::utf8string & util::utf8string::operator=(const std::string & other)
{
  auto splited = splitAsUtf8(other);
  resize(splited.size());
  for (unsigned int i = 0; i < splited.size(); i++)
    (*this)[i] = splited[i];
  return *this;
}

util::utf8string & util::utf8string::operator=(const char * const other)
{
  return operator=(std::string(other));
}

bool util::utf8string::operator==(const std::string & other) const
{
  if (size() != other.size())
    return false;

  for (unsigned int i = 0; i < other.size(); i++)
    if ((*this)[i] != other[i])
      return false;

  return true;
}

void util::utf8string::replace(utf8char from, utf8char to)
{
  std::replace(begin(), end(), from, to);
}

