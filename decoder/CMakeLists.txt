FILE(GLOB SOURCES src/*.cpp)

add_library(decoder STATIC ${SOURCES})
target_link_libraries(decoder reading_machine)
target_link_libraries(decoder Boost)

