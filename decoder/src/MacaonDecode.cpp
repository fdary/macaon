#include "MacaonDecode.hpp"
#include <filesystem>
#include <execution>
#include "util.hpp"
#include "Decoder.hpp"
#include "Submodule.hpp"

po::options_description MacaonDecode::getOptionsDescription()
{
  po::options_description desc("Command-Line Arguments ");

  po::options_description req("Required");
  req.add_options()
    ("model", po::value<std::string>()->required(),
      "Directory containing the trained machine used to decode")
    ("inputTSV", po::value<std::string>(),
      "File containing the text to decode, TSV file")
    ("inputTXT", po::value<std::string>(),
      "File containing the text to decode, raw text file");

  po::options_description opt("Optional");
  opt.add_options()
    ("debug,d", "Print debuging infos on stderr")
    ("silent", "Don't print speed and progress")
    ("reloadEmbeddings", "Reload pretrained embeddings")
    ("lineByLine", "Treat the TXT input as being one different text per line.")
    ("mcd", po::value<std::string>()->default_value("ID,FORM,LEMMA,UPOS,XPOS,FEATS,HEAD,DEPREL"),
      "Comma separated column names that describes the input/output format")
    ("beamSize", po::value<int>()->default_value(1),
      "Size of the beam during beam search")
    ("beamThreshold", po::value<float>()->default_value(0.1),
      "Minimal probability an action must have to be considered in the beam search")
    ("help,h", "Produce this help message");

  desc.add(req).add(opt);

  return desc;
}

po::variables_map MacaonDecode::checkOptions(po::options_description & od)
{
  po::variables_map vm;

  try {po::store(po::parse_command_line(argc, argv, od), vm);}
  catch(std::exception & e) {util::myThrow(e.what());}

  if (vm.count("help"))
  {
    std::stringstream ss;
    ss << od;
    fmt::print(stderr, "{}\n", ss.str());
    exit(0);
  }

  try {po::notify(vm);}
  catch(std::exception& e) {util::myThrow(e.what());}

  if (vm.count("inputTSV") + vm.count("inputTXT") != 1)
  {
    std::stringstream ss;
    ss << od;
    fmt::print(stderr, "Error : one and only one input format must be specified.\n{}\n", ss.str());
    exit(1);
  }

  return vm;
}

int MacaonDecode::main()
{
  auto od = getOptionsDescription();
  auto variables = checkOptions(od);

  std::filesystem::path modelPath(variables["model"].as<std::string>());
  auto machinePath = modelPath / ReadingMachine::defaultMachineFilename;
  auto modelPaths = util::findFilesByExtension(modelPath, fmt::format(ReadingMachine::defaultModelFilename, ""));
  auto inputTSV = variables.count("inputTSV") ? variables["inputTSV"].as<std::string>() : "";
  auto inputTXT = variables.count("inputTXT") ? variables["inputTXT"].as<std::string>() : "";
  auto mcd = variables["mcd"].as<std::string>();
  bool debug = variables.count("debug") == 0 ? false : true;
  bool printAdvancement = !debug && variables.count("silent") == 0 ? true : false;
  bool reloadPretrained = variables.count("reloadEmbeddings") == 0 ? false : true;
  bool lineByLine = variables.count("lineByLine") == 0 ? false : true;
  auto beamSize = variables["beamSize"].as<int>();
  auto beamThreshold = variables["beamThreshold"].as<float>();

  torch::globalContext().setBenchmarkCuDNN(true);
  Submodule::setReloadPretrained(reloadPretrained);

  if (modelPaths.empty())
    util::error(fmt::format("no '{}' files were found, and none were given. Has the model been trained yet ?", fmt::format(ReadingMachine::defaultModelFilename, "")));

  fmt::print(stderr, "Decoding using device : {}\n", NeuralNetworkImpl::getDevice().str());

  try
  {
    ReadingMachine machine(machinePath, false);
    Decoder decoder(machine);

    std::vector<util::utf8string> rawInputs;
    if (!inputTXT.empty())
      rawInputs = util::readFileAsUtf8(inputTXT, lineByLine);

    std::vector<std::vector<std::string>> tsv, noTsv;
    if (!inputTSV.empty())
      tsv = util::readTSV(inputTSV);

    std::vector<BaseConfig> configs;
    if (lineByLine)
    {
      if (rawInputs.size())
        for (unsigned int i = 0; i < rawInputs.size(); i++)
          configs.emplace_back(mcd, noTsv, rawInputs[i], std::vector<int>{(int)i});
      else
        for (unsigned int i = 0; i < tsv.size(); i++)
          configs.emplace_back(mcd, tsv, util::utf8string(), std::vector<int>{(int)i});
    }
    else
    {
      if (rawInputs.size())
        configs.emplace_back(mcd, noTsv, rawInputs[0], std::vector<int>());
      else
        configs.emplace_back(mcd, tsv, util::utf8string(), std::vector<int>());
    }

    machine.setDictsState(Dict::State::Closed);

    if (configs.size() > 1)
    {
      NeuralNetworkImpl::setDevice(torch::kCPU);
      machine.to(NeuralNetworkImpl::getDevice());
      std::for_each(std::execution::seq, configs.begin(), configs.end(),
        [&decoder, debug, printAdvancement, beamSize, beamThreshold](BaseConfig & config)
        {
          decoder.decode(config, beamSize, beamThreshold, debug, printAdvancement);
        });
    }
    else
      decoder.decode(configs[0], beamSize, beamThreshold, debug, printAdvancement);

    for (unsigned int i = 0; i < configs.size(); i++)
      configs[i].print(stdout, i == 0);

  } catch(std::exception & e) {util::error(e);}

  return 0;
}

MacaonDecode::MacaonDecode(int argc, char ** argv) : argc(argc), argv(argv)
{
}

