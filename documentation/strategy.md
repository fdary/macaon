# Strategy

A strategy defines the workflow of its [Reading Machine](readingMachine.md).\
More precisely, it lets you decide the *order* in which predictions ar made (E.g. sentence segmentation before or after POS tagging ?).\
It is flexible enough to allow for *sequential* vs *incremental* modes :
* Sequential : All the text is processed at level *n* (E.g. POS tagging) before begin processed at level *n+1* (E.g. Dependency parsing). It is the most common mode, often called *pipeline* model.
* Incremental : Word *w* is processed at the different levels (E.g. tokenization, pos tagging, attaching it to the syntactic tree...) of analysis, before passing on to word *w+1*.

Example of a strategy (sequential mode : tokenization, pos tagging, morphology tagging, dependency parsing and sentence segmentation) :

```
Strategy
{
  Block : End{cannotMove}
  tokenizer tokenizer ENDWORD 1
  tokenizer tokenizer SPLIT 1
  tokenizer tokenizer * 0 
  Block : End{cannotMove} 
  tagger tagger * 1
  Block : End{cannotMove}
  morpho morpho NOTHING 1
  morpho morpho * 0
  Block : End{cannotMove}
  parser segmenter eager_SHIFT 0
  parser segmenter eager_RIGHT_rel 0
  parser parser * 0
  segmenter parser * 1 
}
```

Here we have a strategy composed of 4 *blocks*.\
Every block is parametrized by its end condition `cannotMove`, which means that the flow of the machine will stay into this block until the condition is met (when the word index has reached the end of the tapes, and cannot move further).\
When the end condition of the current block is met, the word index is reset to 0, and the next block is entered.\
When a new block is entered, the state is set to the origin state of the first defined transition.\
When there is no next block, the analysis is done.

Inside a block are defined the transitions between states.\
Here in the last block we have 4 transitions.\
A transition is defined by `originState destinationState transitionName movement`.\
In the example we have the transition `parser segmenter eager_SHIFT 0`,\
it means that if the current state is `parser` and the classifier makes a prediction of type `eager_SHIFT` (shift in arc-eager TBP) then the new state is segmenter and the word index must not change (0 relative movement).

Here is an example of another strategy (incremental mode : tokenization, pos tagging, morphology tagging, dependency parsing and sentence segmentation) :

```
Strategy
{
  Block : End{cannotMove}
  tokenizer tagger ENDWORD 0
  tokenizer tagger SPLIT 0
  tokenizer tokenizer * 0
  tagger morpho * 0
  morpho parser NOTHING 0
  morpho morpho * 0
  parser segmenter eager_SHIFT 0
  parser segmenter eager_RIGHT_rel 0
  parser parser * 0
  segmenter tokenizer * 1
}
```

There is only one block, because word index is never reset to 0, the work flow only goes to the right one word after another.

