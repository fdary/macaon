# Installation

## Requirements : 
* GNU/Linux OS
* CMake >= 3.16.4
* C++20 compiler such as g++ >= 9.2
* LibTorch version >= 1.5 cxx11 ABI : [link](https://pytorch.org/get-started/locally/)
* Boost >= 1.53.0 with program_options : [link](https://www.boost.org/doc/libs/1_73_0/more/getting_started/unix-variants.html)
* LibTBB 2020 : [link](https://github.com/oneapi-src/oneTBB/releases/tag/v2020.1)

## Download :
`$ git clone https://gitlab.lis-lab.fr/franck.dary/macaon.git`\
`$ cd macaon`\
`$ git submodule init`\
`$ git submodule update`

## Compilation :
`$ cd macaon`\
`$ mkdir build`\
`$ cd build`\
`$ cmake -DCMAKE_INSTALL_PREFIX=/path/to/install ..`\
`$ make -j && make install`


[Back to main page](../README.md)
